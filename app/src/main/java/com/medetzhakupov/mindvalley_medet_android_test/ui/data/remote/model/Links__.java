package com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model;

import java.io.Serializable;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public class Links__ implements Serializable {

    private String self;
    private String html;
    private String download;

    /**
     *
     * @return
     * The self
     */
    public String getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The html
     */
    public String getHtml() {
        return html;
    }

    /**
     *
     * @param html
     * The html
     */
    public void setHtml(String html) {
        this.html = html;
    }

    /**
     *
     * @return
     * The download
     */
    public String getDownload() {
        return download;
    }

    /**
     *
     * @param download
     * The download
     */
    public void setDownload(String download) {
        this.download = download;
    }

}