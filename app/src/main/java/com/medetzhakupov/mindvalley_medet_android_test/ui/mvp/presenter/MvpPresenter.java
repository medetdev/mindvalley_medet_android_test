package com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter;

import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.BaseView;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.MainView;

/**
 * Created by medetzhakupov on 23/08/2016.
 */
public interface MvpPresenter<V extends BaseView> {
    void attachView(V mvpView);

    void detachView();
}
