package com.medetzhakupov.mindvalley_medet_android_test.ui.ui.adapter;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.medetzhakupov.mindvalley_medet_android_test.R;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import mindvalley.MindValley;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private OnPinboardClickListener onPinboardClickListener;
    private Unbinder unbinder;
    private List<Pinboard> pinboards = new ArrayList<>();
    private MindValley mindValley;

    public MyAdapter(MindValley mindValley, OnPinboardClickListener listener){
        this.mindValley = mindValley;
        this.onPinboardClickListener = listener;
    }


    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.my_recyler_item, parent, false);
        final ViewHolder vh = new ViewHolder(view);
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    v.findViewById(R.id.imageView).setTransitionName("trans_image" + pinboards.get((int) v.getTag()).getId());
                    v.findViewById(R.id.userName).setTransitionName("trans_text" + pinboards.get((int) v.getTag()).getId());
                }

                if (onPinboardClickListener != null){
                    onPinboardClickListener.onItemClick(vh.imageView, vh.userName, pinboards.get((int)v.getTag()));
                }
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        mindValley.with(holder.itemView.getContext())
                .setScaleType(ImageView.ScaleType.CENTER_CROP)
                .setLoadingImage(R.drawable.empty_photo)
                .loadImage(pinboards.get(position).getUrls().getSmall(), holder.imageView);
        holder.userName.setText(pinboards.get(position).getUser().getName());
        holder.likes.setText(pinboards.get(position).getLikes() + "");
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return pinboards.size();
    }

    public void setItems(List<Pinboard> pinboardList){
        this.pinboards.clear();
        this.pinboards.addAll(pinboardList);
        notifyDataSetChanged();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        unbinder.unbind();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.imageView) ImageView imageView;
        @BindView(R.id.userName) TextView userName;
        @BindView(R.id.likes) TextView likes;

        ViewHolder(View view){
            super(view);
            unbinder = ButterKnife.bind(this, view);
        }
    }

    public interface  OnPinboardClickListener {
        void onItemClick(ImageView imageView, TextView textView, Pinboard pinboard);
    }
}
