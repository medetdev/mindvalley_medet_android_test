package com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model;

import java.io.Serializable;

/**
 * Created by medetzhakupov on 21/08/16.
 */

public class Category implements Serializable {

    private int id;
    private String title;
    private int photoCount;
    private Links_ links;

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The photoCount
     */
    public int getPhotoCount() {
        return photoCount;
    }

    /**
     *
     * @param photoCount
     * The photo_count
     */
    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    /**
     *
     * @return
     * The links
     */
    public Links_ getLinks() {
        return links;
    }

    /**
     *
     * @param links
     * The links
     */
    public void setLinks(Links_ links) {
        this.links = links;
    }

}