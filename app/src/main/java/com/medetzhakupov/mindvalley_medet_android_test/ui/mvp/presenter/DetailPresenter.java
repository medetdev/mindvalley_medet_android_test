package com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.DetailView;
import com.medetzhakupov.mindvalley_medet_android_test.ui.ui.ViewUtils;
/**
 * Created by medetzhakupov on 21/08/16.
 */
public class DetailPresenter extends BasePresenter<DetailView> {
    Pinboard pinboard;

    public DetailPresenter(Pinboard pinboard) {
        this.pinboard = pinboard;
    }


    @Override
    public void attachView(DetailView view) {
        super.attachView(view);
    }

    public void showDetail(){
        checkViewAttached();
        getView().hideEmptyView();
        getView().showLoading();
        getView().showData(pinboard);
        getView().hideLoading();
    }

    public void saveLink(Context context) {
        ViewUtils.copyToClipboard(context, pinboard.getLinks().getDownload());
    }

    public void openProfile(Activity activity){
        ViewUtils.openLink(activity, pinboard.getUser().getLinks().getHtml());
    }
}
