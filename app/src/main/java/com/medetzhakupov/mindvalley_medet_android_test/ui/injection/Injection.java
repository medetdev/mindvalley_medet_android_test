package com.medetzhakupov.mindvalley_medet_android_test.ui.injection;


import com.medetzhakupov.mindvalley_medet_android_test.ui.data.PinboardRepository;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.PinboardRepositoryImpl;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.PinboardRestService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Injection {

    private static final String BASE_URL = "http://pastebin.com";
    private static OkHttpClient okHttpClient;
    private static PinboardRestService pinboardRestService;
    private static Retrofit retrofitInstance;

    public static void init() {

    }

    public static PinboardRepository providePinboardRepo() {
        return new PinboardRepositoryImpl(providePinboardRestService());
    }

    public static PinboardRestService providePinboardRestService() {
        if (pinboardRestService == null) {
            pinboardRestService = getRetrofitInstance().create(PinboardRestService.class);
        }
        return pinboardRestService;
    }

    public static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            okHttpClient = new OkHttpClient.Builder().addInterceptor(logging).build();
        }

        return okHttpClient;
    }

    public static Retrofit getRetrofitInstance() {
        if (retrofitInstance == null) {
            Retrofit.Builder retrofit = new Retrofit.Builder()
                    .client(Injection.getOkHttpClient())
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
            retrofitInstance = retrofit.build();

        }
        return retrofitInstance;
    }
}
