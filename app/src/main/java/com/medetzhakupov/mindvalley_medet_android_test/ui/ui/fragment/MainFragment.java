package com.medetzhakupov.mindvalley_medet_android_test.ui.ui.fragment;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.medetzhakupov.mindvalley_medet_android_test.R;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;
import com.medetzhakupov.mindvalley_medet_android_test.ui.injection.Injection;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.MainPresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.MainView;
import com.medetzhakupov.mindvalley_medet_android_test.ui.ui.ViewUtils;
import com.medetzhakupov.mindvalley_medet_android_test.ui.ui.adapter.MyAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import mindvalley.MindValley;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment implements MainView {
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.emptyText) TextView textView;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    Unbinder unbinder;
    MyAdapter adapter;
    MindValley mindValley;
    MainPresenter presenter;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mindValley = new MindValley();
        mindValley.setMemoryCache(getActivity().getFragmentManager());
        adapter = new MyAdapter(mindValley, new MyAdapter.OnPinboardClickListener() {
            @Override
            public void onItemClick(ImageView imageView, TextView textView, Pinboard pinboard) {
                DetailFragment detailFragment = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    setExitTransition(TransitionInflater.from(
                            getActivity()).inflateTransition(android.R.transition.fade));
                    setSharedElementReturnTransition(TransitionInflater.from(
                            getActivity()).inflateTransition(R.transition.change_image_transition));

                    detailFragment =
                            DetailFragment.newInstance(pinboard, ViewCompat.getTransitionName(imageView), ViewCompat.getTransitionName(textView));
                    detailFragment.setSharedElementEnterTransition(TransitionInflater.from(
                            getActivity()).inflateTransition(R.transition.change_image_transition));
                    detailFragment.setEnterTransition(TransitionInflater.from(
                            getActivity()).inflateTransition(android.R.transition.fade));
                    detailFragment.setExitTransition(TransitionInflater.from(
                            getActivity()).inflateTransition(android.R.transition.fade));
                    detailFragment.setSharedElementReturnTransition(TransitionInflater.from(
                            getActivity()).inflateTransition(R.transition.change_image_transition));
                } else {
                    detailFragment =
                            DetailFragment.newInstance(pinboard, "", "");
                }

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(DetailFragment.class.getSimpleName())
                        .addSharedElement(imageView, ViewCompat.getTransitionName(imageView))
                        .addSharedElement(textView, ViewCompat.getTransitionName(textView))
                        .replace(R.id.fragment, detailFragment)
                        .commit();
            }
        });

        presenter = new MainPresenter(Injection.providePinboardRepo(), Schedulers.io(), AndroidSchedulers.mainThread());
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_SETTLING){
                    mindValley.setPauseWork(true);
                } else {
                    mindValley.setPauseWork(false);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.fetchData(false);
            }
        });

        if (adapter.getItemCount() == 0){
            presenter.fetchData(false);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showData(List<Pinboard> pinboardList) {
        MyAdapter adapter = (MyAdapter) recyclerView.getAdapter();
        adapter.setItems(pinboardList);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showRefreshLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideRefreshLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showEmptyView() {
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        textView.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        ViewUtils.showShortMessage(getActivity().findViewById(android.R.id.content), message);
    }
}
