package com.medetzhakupov.mindvalley_medet_android_test.ui.data;

import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;

import java.util.List;
import rx.Observable;

/**
 * Created by medetzhakupov on 23/08/2016.
 */

public interface PinboardRepository {
    Observable<List<Pinboard>> fetchPinboard();
}
