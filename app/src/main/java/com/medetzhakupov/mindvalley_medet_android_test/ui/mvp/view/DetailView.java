package com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view;


import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;

import java.util.List;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public interface DetailView extends BaseView {
    void showData(Pinboard pinboard);

    void showLoading();

    void hideLoading();

    void showEmptyView();

    void hideEmptyView();

    void showError(String message);

    void showToastMessage(String message);
}
