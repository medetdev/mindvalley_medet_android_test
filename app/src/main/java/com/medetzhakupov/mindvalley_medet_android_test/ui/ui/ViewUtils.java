package com.medetzhakupov.mindvalley_medet_android_test.ui.ui;

import android.app.Activity;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.medetzhakupov.mindvalley_medet_android_test.R;
import com.medetzhakupov.mindvalley_medet_android_test.ui.ui.customtabs.CustomTabActivityHelper;

/**
 * Created by medetzhakupov on 22/08/2016.
 */

public class ViewUtils {
    public static void openLink(Activity activity, String url) {
        CustomTabActivityHelper.openCustomTab(
                activity,
                new CustomTabsIntent.Builder()
                        .setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                        .addDefaultShareMenuItem()
                        .build(),
                Uri.parse(url));
    }

    public static void showShortMessage(View view, final String message) {
        getSnackBar(view, message, "", null).show();
    }

    public static void showActionMessage(View view, String message, String action, View.OnClickListener listener) {
        getSnackBar(view, message, action, Snackbar.LENGTH_SHORT, listener).show();
    }

    private static Snackbar getSnackBar(View view, String message, String action, View.OnClickListener listener) {
        return getSnackBar(view, message, action, Snackbar.LENGTH_LONG, listener);
    }

    private static Snackbar getSnackBar(View view, String message, String action, int length, View.OnClickListener listener) {
        return Snackbar.make(view, message, length)
                .setAction(action, listener);
    }

    public static void copyToClipboard(Context context, String value) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Service.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied", value);
        clipboard.setPrimaryClip(clip);
        showShortToast("Saved download link to clipboard", context);
    }

    public static void showShortToast(String message, Context context) {
        getToast(message, context, Toast.LENGTH_SHORT).show();
    }

    private static Toast getToast(String message, Context context) {
        return getToast(message, context, Toast.LENGTH_LONG);
    }

    private static Toast getToast(String message, Context context, int length) {
        return Toast.makeText(context, message, length);
    }
}
