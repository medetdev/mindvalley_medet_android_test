package com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote;


import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by medetzhakupov on 23/08/2016.
 */

public interface PinboardRestService {
    @GET("/raw/wgkJgazE")
    Observable<List<Pinboard>> fetchPinboard();
}
