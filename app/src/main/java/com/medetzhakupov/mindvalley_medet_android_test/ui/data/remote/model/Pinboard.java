package com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model;

/**
 * Created by medetzhakupov on 21/08/16.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Pinboard implements Serializable {

    private String id;
    private String createdAt;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean likedByUser;
    private User user;
    private List<Object> currentUserCollections = new ArrayList<Object>();
    private Urls urls;
    private List<Category> categories = new ArrayList<Category>();
    private Links__ links;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The width
     */
    public int getWidth() {
        return width;
    }

    /**
     *
     * @param width
     * The width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     *
     * @return
     * The height
     */
    public int getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     *
     * @return
     * The color
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     * The likes
     */
    public int getLikes() {
        return likes;
    }

    /**
     *
     * @param likes
     * The likes
     */
    public void setLikes(int likes) {
        this.likes = likes;
    }

    /**
     *
     * @return
     * The likedByUser
     */
    public boolean isLikedByUser() {
        return likedByUser;
    }

    /**
     *
     * @param likedByUser
     * The liked_by_user
     */
    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The currentUserCollections
     */
    public List<Object> getCurrentUserCollections() {
        return currentUserCollections;
    }

    /**
     *
     * @param currentUserCollections
     * The current_user_collections
     */
    public void setCurrentUserCollections(List<Object> currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    /**
     *
     * @return
     * The urls
     */
    public Urls getUrls() {
        return urls;
    }

    /**
     *
     * @param urls
     * The urls
     */
    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    /**
     *
     * @return
     * The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     *
     * @param categories
     * The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     *
     * @return
     * The links
     */
    public Links__ getLinks() {
        return links;
    }

    /**
     *
     * @param links
     * The links
     */
    public void setLinks(Links__ links) {
        this.links = links;
    }

}