package com.medetzhakupov.mindvalley_medet_android_test.ui.data;

import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.PinboardRestService;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;

import java.io.IOException;
import java.util.List;
import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

/**
 * Created by medetzhakupov on 23/08/2016.
 */

public class PinboardRepositoryImpl implements PinboardRepository {

    private PinboardRestService pinboardRestService;

    public PinboardRepositoryImpl(PinboardRestService pinboardRestService) {
        this.pinboardRestService = pinboardRestService;
    }

    @Override
    public Observable<List<Pinboard>> fetchPinboard() {
        return Observable
                .defer(new Func0<Observable<List<Pinboard>>>() {
                    @Override
                    public Observable<List<Pinboard>> call() {
                        return pinboardRestService.fetchPinboard();
                    }
                }).retryWhen(new Func1<Observable<? extends Throwable>, Observable<List<Pinboard>>>() {
                    @Override
                    public Observable<List<Pinboard>> call(Observable<? extends Throwable> observable) {
                       return observable.flatMap(new Func1<Throwable, Observable<List<Pinboard>>>() {
                            @Override
                            public Observable<List<Pinboard>> call(Throwable throwable) {
                                if (throwable instanceof IOException){
                                    return Observable.just(null);
                                }
                                return  Observable.error(throwable);
                            }
                        });
                    }
                });
    }
}
