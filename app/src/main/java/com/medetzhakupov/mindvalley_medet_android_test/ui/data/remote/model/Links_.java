package com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model;

import java.io.Serializable;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public class Links_ implements Serializable {

    private String self;
    private String photos;

    /**
     *
     * @return
     * The self
     */
    public String getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The photos
     */
    public String getPhotos() {
        return photos;
    }

    /**
     *
     * @param photos
     * The photos
     */
    public void setPhotos(String photos) {
        this.photos = photos;
    }

}