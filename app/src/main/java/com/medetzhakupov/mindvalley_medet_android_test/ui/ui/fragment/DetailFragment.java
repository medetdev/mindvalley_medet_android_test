package com.medetzhakupov.mindvalley_medet_android_test.ui.ui.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.SharedElementCallback;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.medetzhakupov.mindvalley_medet_android_test.R;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.DetailPresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.DetailView;
import com.medetzhakupov.mindvalley_medet_android_test.ui.ui.ViewUtils;
import com.medetzhakupov.mindvalley_medet_android_test.ui.ui.activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import mindvalley.MindValley;
import mindvalley.Utils;

public class DetailFragment extends Fragment implements DetailView {
    private static final String ARG_PINBOARD = "arg_pinboard";
    public static final String TRANS_IMAGE = "trans_image";
    public static final String TRANS_TEXT = "trans_text";
    DetailPresenter detailPresenter;


    @BindView(R.id.account) ImageButton ivAccount;
    @BindView(R.id.link) ImageButton ivLink;
    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.text) TextView userName;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.emptyText) TextView textView;

    public DetailFragment() {
        // Required empty public constructor
    }

    public static DetailFragment newInstance(Pinboard pinboard, String transImage, String transText) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PINBOARD, pinboard);
        args.putString(TRANS_IMAGE, transImage);
        args.putString(TRANS_TEXT, transText);
        fragment.setArguments(args);
        return fragment;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.link:
                        detailPresenter.saveLink(getContext());
                    break;
                case R.id.account:
                    detailPresenter.openProfile(getActivity());
                    break;
            }

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            detailPresenter = new DetailPresenter((Pinboard) getArguments().getSerializable(ARG_PINBOARD));
            detailPresenter.attachView(this);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, view);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setTransitionName(getArguments().getString(TRANS_IMAGE));
            userName.setTransitionName(getArguments().getString(TRANS_TEXT));
        }

        ivAccount.setOnClickListener(onClickListener);
        ivLink.setOnClickListener(onClickListener);

        detailPresenter.showDetail();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity)getActivity()).appBarLayout.setExpanded(false, true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    public void showData(Pinboard pinboard) {
        final MindValley mindValley = new MindValley();
        mindValley.setScaleType(ImageView.ScaleType.FIT_CENTER);
        mindValley.setMemoryCache(getActivity().getFragmentManager());

        userName.setText(pinboard.getUser().getName());
        mindValley
                .with(getContext()).loadImage(pinboard.getUrls().getSmall(), imageView);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyView() {
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        textView.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        ViewUtils.showShortMessage(getActivity().findViewById(android.R.id.content), message);
    }

    @Override
    public void showToastMessage(String message) {
        ViewUtils.showShortToast(message, getContext());
    }
}
