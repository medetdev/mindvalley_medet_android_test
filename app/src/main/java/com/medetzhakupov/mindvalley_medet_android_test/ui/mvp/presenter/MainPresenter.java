package com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter;


import com.medetzhakupov.mindvalley_medet_android_test.ui.data.PinboardRepository;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.MainView;

import java.util.List;

import rx.Scheduler;
import rx.Subscriber;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public class MainPresenter extends BasePresenter<MainView> {
    private final Scheduler mainScheduler, ioScheduler;
    private PinboardRepository pinboardRepository;

    public MainPresenter(PinboardRepository pinboardRepository, Scheduler ioScheduler, Scheduler mainScheduler) {
        this.pinboardRepository = pinboardRepository;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }


    @Override
    public void attachView(MainView view) {
        super.attachView(view);
    }

    public void fetchData(boolean showRefreshLoading){
        checkViewAttached();
        getView().hideEmptyView();
        if (showRefreshLoading){
            getView().showRefreshLoading();
        } else {
            getView().showLoading();

        }

        addSubscription(pinboardRepository.fetchPinboard().subscribeOn(ioScheduler).observeOn(mainScheduler).subscribe(new Subscriber<List<Pinboard>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getView().showEmptyView();
                getView().hideLoading();
                getView().hideRefreshLoading();
                //By right we should show proper message to the user this is just for testing case
                getView().showError(e.getMessage());
            }

            @Override
            public void onNext(List<Pinboard> pinboards) {
                getView().hideLoading();
                getView().hideRefreshLoading();
                getView().showData(pinboards);
            }
        }));
    }
}
