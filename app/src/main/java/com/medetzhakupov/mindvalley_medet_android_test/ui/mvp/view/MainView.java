package com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view;

import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;

import java.util.List;
import java.util.Objects;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public interface MainView extends BaseView {
    void showData(List<Pinboard> pinboardList);

    void showLoading();

    void hideLoading();

    void showRefreshLoading();

    void hideRefreshLoading();

    void showEmptyView();

    void hideEmptyView();

    void showError(String message);
}
