package mindvalley;

import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public class ConnectionManager {
    private static final String TAG = "ConnectionManager";
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private static ConnectionManager instance;

    public static ConnectionManager getInstance() {
        if (instance == null){
            instance = new ConnectionManager();
        }
        return instance;
    }

    private static OkHttpClient client;

    private ConnectionManager(){
        client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public void getStream(String url, String tag, Callback callback) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .tag(tag)
                .build();
        client.newCall(request).enqueue(callback);
    }

    public String get(String url) throws IOException {
        Response response = null;
        String content = null;
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            response = client.newCall(request).execute();
            content = response.body().string();
        } catch (Exception e){
            Log.e(TAG, e.toString());
        }finally {
            if (response != null) response.close();
        }
        return content;
    }

    public String post(String url, String json) throws IOException {
        Response response = null;
        String content = null;
        try {
            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            content = response.body().string();
        } catch (Exception e){
            Log.e(TAG, e.toString());
        }finally {
            if (response != null) response.close();
        }
        return content;
    }

    public void cancelCallWithTag(String tag) {
        if (client == null) return;

        for(Call call : client.dispatcher().queuedCalls()) {
            if(call.request().tag().equals(tag))
                call.cancel();
        }
        for(Call call : client.dispatcher().runningCalls()) {
            if(call.request().tag().equals(tag))
                call.cancel();
        }
    }

    public boolean isCanceled(String tag) {
        boolean isCanceled = false;
        if (client == null) return isCanceled;

        for(Call call : client.dispatcher().queuedCalls()) {
            if(call.request().tag().equals(tag)){
                isCanceled = call.isCanceled();
                break;
            }
        }
        for(Call call : client.dispatcher().runningCalls()) {
            if(call.request().tag().equals(tag)){
                isCanceled = call.isCanceled();
                break;
            }
        }

        return isCanceled;
    }
}
