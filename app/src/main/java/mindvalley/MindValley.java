package mindvalley;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by medetzhakupov on 21/08/16.
 */
public class MindValley {
    private static final String TAG = "MindValley";
    private Context context;
    private ImageCache imageCache;
    private ImageCache.ImageCacheParams imageCacheParams;
    private int loadingImageRes = -1;
    private boolean fadeInBitmap = true;
    private int fadeInTime = 200;
    private ImageView.ScaleType scaleType = ImageView.ScaleType.CENTER_CROP;
    protected boolean mPauseWork = false;
    private final Object mPauseWorkLock = new Object();

    public MindValley(){
        imageCacheParams = new ImageCache.ImageCacheParams();
    }

    public MindValley with(Context context){
        this.context = context;
        return this;
    }

    public void loadImage(final String url, final ImageView imageView){
        if (TextUtils.isEmpty(url)) throw new NullPointerException("URL was null");

        final WeakReference<ImageView> viewWeakReference = new WeakReference<ImageView>(imageView);
        BitmapDrawable value = null;

        if (imageCache != null){
            value = imageCache.getBitmapFromMemoryCache(url);
        }

        if (value != null){
            setImageDrawable(imageView, value);
        } else {
            Utils.checkDisplaySize(new WeakReference<Context>(context));
            fetchImage(url, url, Utils.displaySize.x/2, Utils.displaySize.y/2)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<BitmapDrawable>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.toString());
                        }

                        @Override
                        public void onNext(BitmapDrawable bitmapDrawable) {
                            if (imageCache != null){
                                imageCache.addBitmapToCache(url, bitmapDrawable);
                            }

                            viewWeakReference.get().setScaleType(scaleType);
                            setImageDrawable(viewWeakReference.get(), bitmapDrawable);
                        }
                    });
        }
    }

    public void setPauseWork(boolean pauseWork) {
        synchronized (mPauseWorkLock) {
            mPauseWork = pauseWork;
            if (!mPauseWork) {
                mPauseWorkLock.notifyAll();
            }
        }
    }

    public MindValley setMemoryCache(FragmentManager fragmentManager){
        addImageCache(fragmentManager, imageCacheParams);
        return this;
    }

    public MindValley setMemoryCacheSize(float percent){
        imageCacheParams.setMemCacheSizePercent(percent);
        return this;
    }

    public MindValley setScaleType(ImageView.ScaleType scaleType) {
        this.scaleType = scaleType;
        return this;
    }

    public void cancelLoad(String url){
        ConnectionManager.getInstance().cancelCallWithTag(url);
    }

    private Observable<BitmapDrawable> fetchImage(final String url, final String tag, final int width, final int height){
       return Observable.create(new Observable.OnSubscribe<BitmapDrawable>() {
           @Override
           public void call(final Subscriber<? super BitmapDrawable> subscriber) {
               try {
                   // Wait here if work is paused and the task is not cancelled
                   synchronized (mPauseWorkLock) {
                       while (mPauseWork && !ConnectionManager.getInstance().isCanceled(url)) {
                           try {
                               mPauseWorkLock.wait();
                           } catch (InterruptedException ignored) {}
                       }
                   }

                   ConnectionManager.getInstance().getStream(url, tag, new Callback() {
                       @Override
                       public void onFailure(Call call, IOException e) {
                           subscriber.onError(e);
                       }

                       @Override
                       public void onResponse(Call call, Response response) throws IOException {
                            if (response.isSuccessful()){
                                Bitmap bitmap = ImageResizer.decodeSampledBitmapFromStream(response.body().byteStream(), width, height, imageCache);
                                response.close();
                                subscriber.onNext(new BitmapDrawable(context.getResources(), bitmap));
                                subscriber.onCompleted();
                            }
                       }
                   });
               } catch (IOException e) {
                   Log.e(TAG, e.toString());
                   Log.e(TAG, e.toString());
                   subscriber.onError(e);
               }
           }
       });
    }

    /**
     * Set placeholder bitmap that shows when the the background thread is running.
     *
     * @param loadingImageRes
     */
    public MindValley setLoadingImage(int loadingImageRes) {
        this.loadingImageRes = loadingImageRes;
        return this;
    }

    public MindValley setFadeInBitmap(boolean fadeInBitmap) {
        this.fadeInBitmap = fadeInBitmap;
        return this;
    }

    public MindValley setFadeInTime(int fadeInTime) {
        this.fadeInTime = fadeInTime;
        return this;
    }

    private void setImageDrawable(ImageView imageView, Drawable drawable) {
        if (fadeInBitmap) {
            // Transition drawable with a transparent drawable and the final drawable
            final TransitionDrawable td =
                    new TransitionDrawable(new Drawable[] {
                            new ColorDrawable(Color.TRANSPARENT),
                            drawable
                    });
            // Set background to loading bitmap
            if (loadingImageRes != -1){
                imageView.setBackgroundResource(loadingImageRes);
            }

            imageView.setImageDrawable(td);
            td.startTransition(fadeInTime);
        } else {
            imageView.setImageDrawable(drawable);
        }
    }

    private void addImageCache(FragmentManager fragmentManager,
                              ImageCache.ImageCacheParams cacheParams) {
        imageCacheParams = cacheParams;
        imageCache = ImageCache.getInstance(fragmentManager, imageCacheParams);
    }
}
