package com.medetzhakupov.mindvalley_medet_android_test;

import com.medetzhakupov.mindvalley_medet_android_test.ui.data.PinboardRepository;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.BasePresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.MainPresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.MainView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by medetzhakupov on 23/08/2016.
 */

public class MainPresenterTest {

    @Mock
    PinboardRepository pinboardRepository;
    @Mock
    MainView mainView;

    MainPresenter mainPresenter;

    @Before
    public void  setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mainPresenter = new MainPresenter(pinboardRepository, Schedulers.immediate(), Schedulers.immediate());
        mainPresenter.attachView(mainView);
    }

    @Test
    public void featchData_returnResult(){
        List<Pinboard> pinboardList = getDummyPinboardList();
        when(pinboardRepository.fetchPinboard()).thenReturn(Observable.just(pinboardList));


        mainPresenter.fetchData(false);


        verify(mainView).hideEmptyView();
        verify(mainView).showLoading();
        verify(mainView).hideLoading();
        verify(mainView).showData(pinboardList);
        verify(mainView, never()).showError(anyString());
    }

    @Test
    public void featchDataError_ErrorMsg(){
        String errorMsg = "Connection problem";
        when(pinboardRepository.fetchPinboard()).thenReturn(Observable.<List<Pinboard>>error(new IOException(errorMsg)));

        mainPresenter.fetchData(false);

        verify(mainView).hideEmptyView();
        verify(mainView).showLoading();
        verify(mainView).hideLoading();
        verify(mainView, never()).showData(anyList());
        verify(mainView).showError(errorMsg);
    }

    @Test(expected = BasePresenter.MvpViewNotAttachedException.class)
    public void mainview_NotAttached_ThrowsMvpException() {
        mainPresenter.detachView();

        mainPresenter.fetchData(false);

        verify(mainView, never()).showLoading();
        verify(mainView, never()).showData(anyList());
    }

    private List<Pinboard> getDummyPinboardList() {
        return null;
    }

}
