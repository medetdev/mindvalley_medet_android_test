package com.medetzhakupov.mindvalley_medet_android_test;

import com.medetzhakupov.mindvalley_medet_android_test.ui.data.PinboardRepository;
import com.medetzhakupov.mindvalley_medet_android_test.ui.data.remote.model.Pinboard;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.BasePresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.DetailPresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.presenter.MainPresenter;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.DetailView;
import com.medetzhakupov.mindvalley_medet_android_test.ui.mvp.view.MainView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by medetzhakupov on 23/08/2016.
 */

public class DetailsPresenterTest {


    @Mock
    DetailView detailView;

    DetailPresenter detailPresenter;

    @Before
    public void  setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        detailPresenter = new DetailPresenter(new Pinboard());
        detailPresenter.attachView(detailView);
    }

    @Test(expected = BasePresenter.MvpViewNotAttachedException.class)
    public void detailview_NotAttached_ThrowsMvpException() {
        detailPresenter.detachView();

        detailPresenter.showDetail();

        verify(detailView, never()).showLoading();
        verify(detailView, never()).showData(new Pinboard());
    }
}
